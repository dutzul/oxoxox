	#include <bits/stdc++.h>	
	using namespace std;

	#define sh short int
	#define LE 3000

	int H[LE][LE];
	int H2[LE][LE];
	int grad[LE];
	int result[LE];
	double sum_inside_hot=0;
	int n_global;

	inline bool is_line(int a,int b,int c,int d){
		return ((a!=b&&a!=c&&a!=d&&b!=c&&b!=d&&c!=d)&&(H[b][d]==false&&H[a][d]==false&&H[a][c]==false));
	}

	std::vector<long long> pw;
	#define MOD (long long)999998727899999
	inline long long get_hash_from_vec(std::vector<sh>& arr){
		long long result=0;

		for(int el: arr){
			result=(result+pw[el]);
		}

		return result%MOD;
	}

	std::vector<int> edges[LE];

	vector<int> calc_sum_neigh(vector<sh>& arr,std::vector<int>& nodes){
		vector<bool> vec(n_global+1);

		std::vector<int> result;
		for(int el:arr){
			for(int j:edges[el]){
				vec[j]=true;
			}
		}
		for(int el:arr){
			vec[el]=false;
		}

		for(int i:nodes){
			if (vec[i]==true){
				result.push_back(i);
			}
		}
		return result;
	}

	std::vector<int> randnum;
	std::vector<int> range_rand;

	void init_random(int n){
		std::srand(std::time(0));

		for(int i=0;i<n;++i){
			for(int j=1;j<=n-i+5;++j)
				randnum.push_back(i);
			range_rand.push_back(randnum.size());
		}
	}

	int get_random(int maxn){
		int res=randnum[rand()%range_rand[maxn]];
		if (res>maxn){
			cerr<<"ISBAD-----> "<<maxn<<"       "<<res<<endl;
		}
		return  res;;
	}


	int get_average(vector< pair<sh,vector<sh> > >& sol2){
		int  sum=0;
		for(int i=1;i<=200;++i){
			sum+=sol2[rand()%sol2.size()].first;
		}
		return sum/200;
	}

	void clear_vec(vector< pair<sh,vector<sh> > >& sol2,unordered_set<long long> & myHash){
		
		int avg=get_average(sol2);
		int right=sol2.size()-1;

		for(int i=0;i<right-1;++i){
			if (sol2[i].first>=avg){
				swap(sol2[i],sol2[right]);
				long long hash =get_hash_from_vec(sol2[i].second);
				myHash.erase(hash);
				--right;
			}
		}

		int to_erase=sol2.size()-right;
		while (to_erase--){
			sol2.pop_back();
		}

	}

	vector<int> solve(std::vector<int> nodes){
		const int MAXLEN=100000;
		vector< pair<sh,vector<sh> > >  sol1;
		vector< pair<sh,vector<sh> > > sol2;
		

		for(int i=0;i<=MAXLEN;++i){
			vector<sh> aux={(sh)nodes[get_random(nodes.size()-1)]};
			sol1.push_back(make_pair(calc_sum_neigh(aux,nodes).size(),aux));
		}

		int n=nodes.size();

		for(int step=1;step<=n;++step){
			cerr<<step<<endl;
			cerr<<sol1.size()<<'\n';
			cerr<<endl<<"PULA_IN   "<<sol1[0].second.size()<<endl;
			int number=0;

			unordered_set<long long> myHash;

			for(auto graph:sol1){
				number++;
				if (number%10000==0){
					cerr<<"YOYO"<<endl;
				}
				
				std::vector<int> neigh=calc_sum_neigh(graph.second,nodes);
				
				vector<bool> adj(n_global+1);
				for(auto el:neigh){
					adj[el]=true;
				}

				vector<bool> already_added(n_global+1);
				for(int el:graph.second){
					already_added[el]=true;
				}

				vector<int> not_used;
				for(int el:nodes){
					if (already_added[el]==false){
						not_used.push_back(el);
					}
				}

				vector<int> not_used_and_noadj;
				
				for(int el:not_used){
					if (adj[el]==false){
						not_used_and_noadj.push_back(el);
					}
				}

				vector<int>not_used_and_adj;
				for(int el:not_used){
					if (adj[el]==true){
						not_used_and_adj.push_back(el);
					}
				}


				int nr_sol=0;
				if (not_used_and_noadj.size()==0) continue;
				
				int to_mul=5;
				int nr=0;


				if (number%1000==1)
					cerr<<"XXXXXXXXXXXXXXXX 	"<<not_used_and_noadj.size()*not_used_and_noadj.size()*not_used.size()*not_used.size()<<'\n';

				if (not_used_and_noadj.size()*not_used_and_noadj.size()*not_used.size()*not_used.size()<100000){
				//	if (nr==0)cerr<<"ENTERED"<<endl;
					nr=1;
					for(auto a:not_used_and_noadj)
						for(auto d:not_used_and_noadj)
							for(auto b: not_used)
								for(auto c:not_used)
									if(is_line(a,b,c,d)){
	//++nr;
										std::vector<sh> graphnew(graph.second);
										graphnew.insert(graphnew.end(), {a,b,c,d});

										long long hash=get_hash_from_vec(graphnew);
							
										if (myHash.find(hash)!=myHash.end()){
											continue;
										}else{
											myHash.insert(hash);
										}
										++nr_sol;
							
										int size_neigh_used=neigh.size();
										for(int el:not_used_and_noadj){
											if (el!=a&&el!=b&&el!=c&&el!=d)
												size_neigh_used+=H[el][a]|H[el][b]|H[el][c]|H[el][d];
										}

										sol2.push_back(make_pair(size_neigh_used,graphnew));

										double percentage=pow((((double)(sol1.size()-number))/((double)sol1.size())*5),4.0);
										if (graph.second.size()<=1000){
											percentage=percentage*to_mul+10*to_mul;
										}
										if (nr_sol>(int)(percentage)+3){
											break;
										}

									}
				}else{

					for(int xx=1;xx<2000*to_mul;++xx){

						sh a=not_used_and_noadj[get_random(not_used_and_noadj.size()-1)];
						sh d=not_used_and_noadj[get_random(not_used_and_noadj.size()-1)];
						sh b=not_used[get_random(not_used.size()-1)];
						sh c=not_used[get_random(not_used.size()-1)];

						int a2=a,b2=b,c2=c,d2=d;

						if(is_line(a,b,c,d)){
							a2=a;
							b2=b;
							c2=c;
							d2=d;
						}

						if (is_line(d,b,c,a)){
							a2=d;
							b2=b;
							c2=c;
							d2=a;
						}

						if (is_line(a,c,b,d)){
							a2=a;
							b2=c;
							c2=b;
							d2=d;
						}
		///////////////////////////////////////////////////////////
						if (is_line(b,d,a,c)){
							a2=b;
							b2=d;
							c2=a;
							d2=c;
						}

						if (is_line(c,d,a,b)){
							a2=c;
							b2=d;
							c2=a;
							d2=b;
						}


						if (is_line(b,a,d,c)){
							a2=b;
							b2=a;
							c2=d;
							d2=c;
						}
					
					
						a=a2;
						b=b2;
						c=c2;
						d=d2;

						if (is_line(a,b,c,d)){
							
							std::vector<sh> graphnew(graph.second);
							graphnew.insert(graphnew.end(), {a,b,c,d});

							long long hash=get_hash_from_vec(graphnew);
				
							if (myHash.find(hash)!=myHash.end()){
								continue;
							}else{
								myHash.insert(hash);
							}
							++nr_sol;
				
							int size_neigh_used=neigh.size();
							for(int el:not_used_and_noadj){
								if (el!=a&&el!=b&&el!=c&&el!=d)
									size_neigh_used+=H[el][a]|H[el][b]|H[el][c]|H[el][d];
							}

							sol2.push_back(make_pair(size_neigh_used,graphnew));

							double percentage=pow((((double)(sol1.size()-number))/((double)sol1.size())*5),4.0);
							if (graph.second.size()<=1000){
								percentage=percentage*to_mul+10*to_mul;
							}
							if (nr_sol>(int)(percentage)+3){
								break;
							}
						}
					}
				}
				clock_t begin = clock();
				while(sol2.size()>MAXLEN*5){
					clear_vec(sol2,myHash);
				}
				clock_t end = clock();
				double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
				sum_inside_hot+=time_spent;
				
			}

			cerr<<"YESEND"<<endl;
			if (sol2.empty()){
				break;
			}


			swap(sol1,sol2);
			sol2.clear();			
			std::vector<pair<int,int> > aux;
			for(int i=0;i<sol1.size();++i){
				aux.push_back(make_pair(sol1[i].first,i));
			}
			sort(aux.begin(),aux.end());
			for(auto el:aux){
				sol2.push_back(sol1[el.second]);
			}

			swap(sol2,sol1);
			sol2.clear();

			while (sol1.size()>MAXLEN){
				sol1.pop_back();
			}

			cerr<<"--------->"<<sol1[0].first+sol1[0].second.size()<<"      "<<sol1[sol1.size()-1].first+sol1[0].second.size()<<endl;
		}

		std::vector<int> res;
		for(auto el:sol1[0].second){
			res.push_back((int)el);
		}
		return res;
	}

	void complete_solution(std::vector<int> &nodes,std::vector<int>& sol,int n){

		cerr<<"SOL SIZE "<<sol.size()<<'\n';
		for(int i=0;i<nodes.size();i+=2){
			sol.push_back(n+1);
			sol.push_back(nodes[i]);
			sol.push_back(nodes[i+1]);
			sol.push_back(n+2);
			n+=2;
		}

		cerr<<"SOL SIZE "<<sol.size()<<endl;
	}

	void get_solution(vector<int> vec){
		result[vec[0]]=vec[0];

		for(int i=1;i<vec.size();i+=4){
			H[vec[i]][vec[i+1]]=H[vec[i+1]][vec[i+2]]=H[vec[i+2]][vec[i+3]]=true;

			int c0=0;
			int c1=0;
			int c2=0;
			int c3=0;

			for(int j=0;j<i;++j){
				if (H[vec[j]][vec[i]]) c0=1;
				if (H[vec[j]][vec[i+1]]) c1=1;
				if (H[vec[j]][vec[i+2]]) c2=1;
				if (H[vec[j]][vec[i+3]]) c3=1;
			}

			if (c0+c1+c2+c3>2){
				cerr<<"SOLUTIE PROASTA"<<endl;
				cerr<<i<<endl;
				break;
			}
				

			if (c0+c1+c2+c3==1){
				if (c0==1||c3==1){
					c0=1;
					c3=1;
				}else{
					c1=1;
					c2=1;
				}
			}

			if ((c0+c1+c2+c3)==0){
				cerr<<"AICISHA  0";
				c1=1;
				c2=1;
			}


			for(int j=0;j<i;++j){
				if (c0) H[vec[j]][vec[i]]=true;
				if (c1) H[vec[j]][vec[i+1]]=true;
				if (c2) H[vec[j]][vec[i+2]]=true;
				if (c3) H[vec[j]][vec[i+3]]=true;
			}

			int a_saved=vec[i];
			int b_saved=vec[i+1];
			int c_saved=vec[i+2];
			int d_saved=vec[i+3];
			result[a_saved]=c_saved;
			result[b_saved]=a_saved;
			result[c_saved]=d_saved;
			result[d_saved]=b_saved;
		}
	}

	bool comp(int i1,int i2){
		return grad[i1]<grad[i2];
	}

	void init_hash(int n){
		pw.push_back(1);
		for(int i=1;i<=n;++i){
			pw.push_back((pw[i-1]*(long long)n)%MOD);
		}
	}


	int main(){
		ifstream f("wow.in");

		int n,m;
		f>>n>>m;
		n_global=n;

		//n+=30;
		

		init_random(n);
		init_hash(n);

		
		for(int i=1;i<=m;++i){
			int xx,yy;
			f>>xx>>yy;
			H[xx][yy]=true;
			H[yy][xx]=true;
			H2[xx][yy]=true;
			H2[yy][xx]=true;
			edges[xx].push_back(yy);
			edges[yy].push_back(xx);
			grad[xx]++;
			grad[yy]++;
		}

		std::vector<int> nodes;
		for(int i=1;i<=n;++i){
			nodes.push_back(i);
		}

		sort(nodes.begin(),nodes.end(),comp);


	clock_t begin = clock();
		std::vector<int> result_vec=solve(nodes);
	clock_t end = clock();


	double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	cerr<<"TIME PERCENTAGE SPENT INSIDE HOT "<<(sum_inside_hot/time_spent)*100<<'\n';


	std::vector<int > others;

	for(int i=1;i<=n;++i){
		if (std::find(result_vec.begin(),result_vec.end(),i)==result_vec.end()){
			others.push_back(i);
		}
	}


	cerr<<"------"<<endl;
	for(auto el:result_vec){
		cerr<<el<<" ";
	}
	cerr<<endl<<"-------"<<endl;
	if (others.size()%2){
		others.push_back(++n);	
	}
	complete_solution(others,result_vec,n);


	get_solution(result_vec);

		cerr<<result_vec.size()<<'\n';

		cerr<<endl<<endl;
		
		int n2=result_vec.size();


		set<pair<int,int>> extra_edges;
		for(int i=1;i<=n2;++i){
			for(int j=1;j<=n2;++j){
				if (H2[i][j]!=H[i][j]){
					extra_edges.insert(make_pair(i,j));
				}
			}
		}

		cout<<n2-n_global<<" "<<extra_edges.size()<<'\n';
		for(auto edge : extra_edges){
			cout<<edge.first<<" "<<edge.second<<'\n';
		}

		for(int i=1;i<=n2;++i){
			cout<<result[i]<<'\n';
		}


		return 0;
	}




						
